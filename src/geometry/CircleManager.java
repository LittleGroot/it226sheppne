package geometry;

import java.util.Scanner;

public class CircleManager {
	
	public void circleInterface(Scanner Sc)
	{
		
		System.out.println("\nWhat would you like to calculate?");
		System.out.println("Area");
		System.out.println("Perimeter");
		System.out.println("Type Done to exit");
		
		String shape = Sc.nextLine();
		shape = shape.toLowerCase();
		Boolean isDone = false;
		
		while (isDone == false)
		{
			switch (shape){
			case "area":
					Area(Sc);
					break;
					
			case "perimeter":
					Perimeter(Sc);
					break;
					
			case "done":
				//exits while by changing isDone
				isDone = true;
				break;
			default:
				System.out.println("Does not Match, please type again!");
				break;
			}
			

			if(isDone==false)
			{
				
				
				System.out.println("\nWhat would you like to calculate?");
				System.out.println("Area");
				System.out.println("Perimeter");
				System.out.println("Type Done to exit");
				Sc.nextLine();
				shape = Sc.nextLine();
				shape = shape.toLowerCase();
			}

		}
	}
	
	
	
	public static void Perimeter(Scanner Sc)
	{
		System.out.println("\nPlease enter the radius of the circle");
		double radius = Sc.nextDouble();
		System.out.println("Answer = " + Math.PI*radius*2);
		
	}
	
	
	public static void Area(Scanner Sc)
	{
		System.out.println("\nPlease enter the radius of the circle");
		double radius = Sc.nextDouble();
		System.out.println("Answer = " + Math.PI*Math.pow(radius, 2));
		
	}
}
