package geometry;

import java.util.Scanner;

public class TriangleManager {
	
	public void triangleInterface(Scanner Sc)
	{
		
		System.out.println("\nWhat would you like to calculate?");
		System.out.println("Area");
		System.out.println("Perimeter");
		System.out.println("Type Done to exit");
		
		String shape = Sc.nextLine();
		shape = shape.toLowerCase();
		Boolean isDone = false;
		
		while (isDone == false)
		{
			switch (shape){
			case "area":
					Area(Sc);
					break;
					
			case "perimeter":
					Perimeter(Sc);
					break;
					
			case "done":
				//exits while by changing isDone
				isDone = true;
				break;
			default:
				System.out.println("Does not Match, please type again!");
				break;
			}
			

			if(isDone==false)
			{
				
				
				System.out.println("\nWhat would you like to calculate?");
				System.out.println("Area");
				System.out.println("Perimeter");
				System.out.println("Type Done to exit");
				Sc.nextLine();
				shape = Sc.nextLine();
				shape = shape.toLowerCase();
			}

		}
	}
	
	
	
	public static void Perimeter(Scanner Sc)
	{
		System.out.println("\nPlease enter side1");
		double s1 = Sc.nextDouble();
		System.out.println("Please enter side2");
		double s2 = Sc.nextDouble();
		System.out.println("Please enter side3");
		double s3 = Sc.nextDouble();
		System.out.println("answer = " + (s1+s2+s3));
	}
	
	
	public static void Area(Scanner Sc)
	{
		System.out.println("\nPlease enter side1");
		double s1 = Sc.nextDouble();
		System.out.println("Please enter side2");
		double s2 = Sc.nextDouble();
		System.out.println("Please enter side3");
		double s3 = Sc.nextDouble();
		
		double S = (s1+s2+s3)/2;
		double area = Math.sqrt((S)*(S-s1)*(S-s2)*(S-s3));
		
		System.out.println("Answer = " + area);
	}
}
