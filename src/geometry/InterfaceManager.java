package geometry;

import java.util.Scanner;

public class InterfaceManager {

	public static void main(String[] args) {
		Interface();
	}
	public static void Interface()
	{
		Scanner Sc = new Scanner(System.in);
		RectangleManager RM = new RectangleManager();
		TriangleManager TM = new TriangleManager();
		CircleManager CM = new CircleManager();
		
		System.out.println("Hello, this is a Geometry Helper Program.");
		System.out.println("Option one: Rectangles");
		System.out.println("Option two: Triangles");
		System.out.println("Option three: Circles");
		System.out.println("Type Done to exit");
		
		String shape = Sc.nextLine();
		shape = shape.toLowerCase();
		Boolean isDone = false;
		
		while (isDone == false)
		{
			switch (shape){
			case "rectangles":
				RM.rectangleInteface(Sc);
				break;
			
			case "triangles":
				TM.triangleInterface(Sc);
				break;
				
			case "circles":
				CM.circleInterface(Sc);
				break;
			
			case "done":
				//exits while by changing isDone
				isDone = true;
				break;
			default:
				System.out.println("Does not Match, please type again!");
				break;
			}
			
		
			if(isDone==false)
			{
				System.out.println();
				System.out.println("Hello, this is a Geometry Helper Program.");
				System.out.println("Option one: Rectangles");
				System.out.println("Option two: Triangles");
				System.out.println("Option three: Circles");
				System.out.println("Type Done to exit");
				
				shape = Sc.nextLine();
				shape = shape.toLowerCase();
			}

		}
		
	}
}
